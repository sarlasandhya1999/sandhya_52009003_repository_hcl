package com.greatlearning.Ass;

public class HrDepartment extends SuperDepartment {
	public String departmentName() {
		   return "HR Department";
		}
	public String getTodaysWork() {
		    return "Fill todays worksheet and mark your attendance";
		}
		
	public String getWorkDeadline() {
		    return "Complete by EOD";
		}
	
	public String doActivity() {
		    return "Team Lunch";
		}
	@Override
	public String toString() {
		return "HrDepartment []";
	}
		
}

package com.greatlearning.Ass;

public class TechDepartment extends SuperDepartment {
	public String departmentName() {
	    return "Tech Department";
	}
	@Override
	public String toString() {
		return "TechDepartment []";
	}
	public String getTodaysWork() {
	    return "Complete coding of module 1";
	}
	
	public String getWorkDeadline() {
	    return "Complete by EOD";
	}

	public String gettechStackInformation() {
	    return "core Java";
	}
	

}

package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bean.LikedBooks;
import com.bean.LikedBooksCK;

@Repository
public interface LikedBooksDao extends JpaRepository<LikedBooks,LikedBooksCK>  {

}

package com.bean;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class LikedBooks {	
	
 private String title;
 private String author;
 private float price;
 @EmbeddedId
 LikedBooksCK likedBooksCK;
 
public LikedBooks() {
	super();
	// TODO Auto-generated constructor stub
}
public LikedBooks(String title, String author, float price, LikedBooksCK likedBooksCK) {
	super();
	this.title = title;
	this.author = author;
	this.price = price;
	this.likedBooksCK = likedBooksCK;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getAuthor() {
	return author;
}
public void setAuthor(String author) {
	this.author = author;
}
public float getPrice() {
	return price;
}
public void setPrice(float price) {
	this.price = price;
}
public LikedBooksCK getLikedBooksCK() {
	return likedBooksCK;
}
public void setLikedBooksCK(LikedBooksCK likedBooksCK) {
	this.likedBooksCK = likedBooksCK;
}
 
 
}

import { Component, OnInit } from '@angular/core';
import { Books } from '../books';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
books:Array<Books>=[];
adminFlag:boolean=false;
name:string=''
flag:boolean=false
updateFlag:boolean =false
bookId:number=0;
updatenameFlag:boolean =false
updateAuthorFlag:boolean =false
updatePriceFlag:boolean =false
updateGenreFlag:boolean =false
updateRatingFlag:boolean =false

  constructor(
    public productService:BooksService,
   

  ) { }

  ngOnInit(): void {
    this.loadBooks()
     let obj1 =sessionStorage.getItem("name");
     let obj2 =sessionStorage.getItem("adminFlag")
    if(obj1!=null){
      this.name= obj1
      sessionStorage.setItem("name",this.name)
      this.flag=true
       }
       if(obj2!=null&& obj2 == "1"){
        this.adminFlag=true
     }
    
  }
  loadBooks(){
    // console.log("event");
    this.productService.loadAllBooks().subscribe(
      res=>this.books=res,
      error=>console.log(error),
       )
}
updateBtn(bid:number){
  console.log("event update")
  this.updateFlag =true
  this.bookId=bid
  
}
addBookBtn(){
  console.log("add event")
}
updateBookNameBtn(){
  console.log(this.bookId)
}
updateAuthorBtn(){
  console.log(this.bookId)
}
updatePriceBtn(){
  console.log(this.bookId)
}
updateRatingBtn(){
  console.log(this.bookId)
}
updateGenreBtn(){
  console.log(this.bookId)
}
deleteBook(bid:number){
  console.log("event delete")
}
}

import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { Books } from './books';
@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(
    public http:HttpClient
  ) { }

  loadAllBooks(): Observable<Books[]>{
return this.http.get<Books[]>("http://localhost:8181/book/getAllBooks")
  }
}

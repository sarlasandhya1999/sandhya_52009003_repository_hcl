package com.controller.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.bean.Books;

class BooksControllerTest {
String baseUrl ="http://localhost:8181/book";
	@Test
	void testGetAllBooks() {
		//fail("Not yet implemented");
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<List<Books>> books = restTemplate.exchange(baseUrl+"/getAllBooks",HttpMethod.GET,null,new ParameterizedTypeReference<List<Books>>() {});
		List<Books>listOfBooks=books.getBody();
		Assertions.assertTrue(listOfBooks.stream().count()==5);
	}

	@Test
	void testGetBookById() {
		//fail("Not yet implemented");
		RestTemplate restTemplate = new RestTemplate();
		Books books = restTemplate.getForObject(baseUrl+"/getBookById/101", Books.class);
		//ResponseEntity<Books> books = restTemplate.exchange(baseUrl+"/getAllBooks",HttpMethod.GET,null,new ParameterizedTypeReference<Books>() {});
		
		Assertions.assertEquals("Wings of Fire", books.getBookName());
		
	}

	@Test
	void testStoreBooks() {
		//fail("Not yet implemented");
		RestTemplate restTemplate = new RestTemplate();
		
		Books books =new  Books();
		books.setBookId(105);
		books.setBookName("What-If");
		books.setAuthor(" Randall Munroe");
		books.setBookGenre("Humor");
		books.setBookImageUrl("https://d3525k1ryd2155.cloudfront.net/h/847/615/1384615847.0.x.jpg");
		books.setBookPrice(260);
		books.setBookRating(4.5f);
		
		String result = restTemplate.postForObject(baseUrl+"/storeBooks",books, String.class);
		Assertions.assertEquals("Book " + books.getBookId() + " stored successfully", result);
		Assertions.assertEquals("Book id must be unique", result);
		
	}

	@Test
	void testDeleteBook() {
		//fail("Not yet implemented");
		
	}

}

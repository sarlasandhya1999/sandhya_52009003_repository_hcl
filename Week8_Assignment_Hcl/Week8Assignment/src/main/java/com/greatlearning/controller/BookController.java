package com.greatlearning.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.greatlearning.bean.Book;
import com.greatlearning.service.BookService;

@Controller

public class BookController {
	@Autowired
	BookService bookService;

	@GetMapping(value = "display")
	public ModelAndView getAllBook(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		List<Book> listOfBook = bookService.getAllBook();
		req.setAttribute("products", listOfBook);
		req.setAttribute("msg", "All Book Details");
		mav.setViewName("display.jsp");
		return mav;
	}
	
	@GetMapping(value = "like")
	public ModelAndView likeProduct(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(req.getParameter("id"));
		System.out.println("Book id is "+id);
		List<Book> listOfBook = bookService.getAllBook();
		req.setAttribute("products", listOfBook);
		req.setAttribute("msg", "All Book Details");
		mav.setViewName("like.jsp");
		return mav;
	}

	@RequestMapping(value = "check",method = RequestMethod.POST)
	public ModelAndView userLoginDetails(HttpServletRequest req) {  
		String user = req.getParameter("username");
		String pass = req.getParameter("password");
		
		ModelAndView mav = new ModelAndView();
		if(user.equals("username") && pass.equals("password")) {
			mav.setViewName("success.jsp");
		}else {
			mav.setViewName("failure.jsp");
		}
		return mav;
	}


}

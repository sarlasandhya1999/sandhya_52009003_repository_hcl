package com.greatlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.bean.Book;
import com.greatlearning.bean.User;
import com.greatlearning.dao.BookDao;

@Service


public class BookService {
	@Autowired
	BookDao bookDao;
	
	public List<Book> getAllBook() {
		return bookDao.getAllBook();
	}
    
	public List<User>getAllUser(){
		return bookDao.getAllUser();
		
	}


}

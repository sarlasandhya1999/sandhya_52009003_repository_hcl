create database week8;

use week8;



create table book(id int primary key,name varchar(100),genre varchar(100),url varchar(1000));


insert into book values(100,'The Dark Road','Novel','https://upload.wikimedia.org/wikipedia/en/1/15/Dark_Road_%28novel%29.jpg');

insert into book values(101, 'Wings of fire','Autobiography','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQUzzuGBPYD63LKpNxlkrWNiq5usJNwVUmsHQ&usqp=CAU');

insert into book values(102,'The Secret',' self-help','https://upload.wikimedia.org/wikipedia/en/thumb/0/02/TheSecretLogo.jpg/220px-TheSecretLogo.jpg');

insert into book values(103,'Kuvempu','Fiction','https://rukminim1.flixcart.com/image/612/612/khz693k0-0/regionalbooks/x/q/r/the-bride-in-the-rainy-mountains-malegalalli-madumagalu-original-imafxvbbbgzysyw3.jpeg?q=70');

insert into book values(104,'The Lowland','Novel','https://s.wsj.net/public/resources/images/OB-ZB616_ijhump_DV_20130927053103.jpg');



create table user(username varchar(100),password varchar(100));


insert into user values('Sandhya','123');

insert into user values('Sanju','1234');  



create table register(username varchar(100),password varchar(100));


create table bookliked(id int,name varchar(100),genre varchar(100),url varchar(100));

create table readlater(id int,name varchar(100),genre varchar(100),url varchar(100));

package com.greatlearning.week7.assignment.controller;

import java.io.IOException;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.greatlearning.week7.assignment.bean.Login;
import com.greatlearning.week7.assignment.dao.LoginDetailsDao;

/**
 * Servlet implementation class ReadLaterController
 */
@WebServlet("/ReadLaterController")
public class ReadLaterController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static Login currentUser;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReadLaterController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String id = request.getParameter("laterbookid");
		String name=request.getParameter("name");
		String password=request.getParameter("password");
		Login ld=new Login();
		ld.setName(name);
		ld.setPassword(password);
		try {
			if(LoginDetailsDao.validate(ld)) {
				currentUser.setName(name);
				currentUser.setPassword(password);
				response.sendRedirect("success.jsp");
			}else {
				HttpSession session=request.getSession();
				RequestDispatcher rd=request.getRequestDispatcher("readlater.jsp");
				
			}
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<html><body><h1>Book added successfully to Read later section</h1></body></html>");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		

		
	}
	}




    
package com.hcl.miniproject.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.hcl.miniproject.pojo.LoginUser;

public interface UserRepository extends CrudRepository<LoginUser, Integer>{
	List<LoginUser> findAll();
	
	LoginUser findByEmail(String email);
	
	LoginUser findById(int id);
}
